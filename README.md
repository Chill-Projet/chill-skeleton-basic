Chill - Standard Project
========================

This is the basic project you must install first in order to install Chill software.

The installation process is documented here: http://docs.chill.social/en/latest/installation/index.html (current versions lives at https://gitlab.com/Chill-Projet/chill-bundles/-/tree/master/docs)

---

## Installation

Suivre la procédure https://chill.readthedocs.io/en/latest/installation/index.html

En cas de problème évoquant `VCS` et `vendor/chill-project/chill-bundles` faire un git
clone de `https://gitlab.com/Chill-Projet/chill-bundles` vers le répertoire
`vendor/chill-project/chill-bundles`.


### Migrations de la db

Les fichiers de migrations qui sont dans les modules Chill sont maintenant automatiquement détectés par Symfony.

`php bin/console doctrine:migrations:migrate`


### Chargement des fixtures

Pour charger des fixtures, vous pouvez utiliser la commande
`php bin/console doctrine:fixtures:load` qui peuple la db pour le développement.

Attention il y a besoin d'une connexion internet pour `Geographical Unit`.

#### Erreur connue : pas de données modifiées dans la db

Augmenter le `memory_limit` afin d'arriver à la fin de la procédure (sinon la procédure s'arrête
sans message d'erreur).

Actuellement les dernières fixtures à charger sont :

```
   > loading Chill\PersonBundle\DataFixtures\ORM\LoadAccompanyingPeriodNotifications
   > loading Chill\PersonBundle\DataFixtures\ORM\LoadAccompanyingPeriodWork
   > loading Chill\ActivityBundle\DataFixtures\ORM\LoadActivityNotifications
   > loading Chill\AsideActivityBundle\DataFixtures\ORM\LoadAsideActivity
   > loading Chill\ThirdPartyBundle\DataFixtures\ORM\LoadThirdParty
   > loading App\DataFixtures\LoadHouseholdAddress
```

## Enable collabora

The file `docker-compose.dev.yml` add configuration for use with collabora CODE. To use this, you must add those lines to your `/etc/hosts` file:

```
127.0.0.1       nginx collabora
```

Then, you call:

* http://nginx:8001 to get the main chill app;
* http://collabora:9980 to get collabora

The collabora's admin panel is reachable at http://collabora:9980/loleaflet/dist/admin/admin.html


#### Alternative: chargement d'un dump de la db

Pour importer directement un sql peuplé pour le développement, un `postgres.db.sql` se trouve à la racine du dépôt.

```
$ docker cp postgres.db.sql fb9290290f50:/tmp/
$ docker-compose exec --user postgres db bash
postgres@fb9290290f50:/$ psql postgres < /tmp/postgres.db.sql
```

où fb9290290f50 est l'id du container de la db (de l'image postgres:12).


## Gérer le problème de memory_limit de manière générale

- générer une copie du `php.ini` (`docker cp XXX_CONTAINER_PHP_ID_XXX:/usr/local/etc/php/php.ini-development php.ini-dev`)
- éditer le fichier et choisissez la valeur du paramètre `memory_limit`  (par exemple `memory_limit = 5G`)
- mettre à jour le docker-compose.yml afin que la copie modifiée du `php.ini` écrase  `/usr/local/etc/php/php.ini` :

  ```
  services:
  php: &php-definition
    ...
    volumes:
      ...
      - ./php.ini-dev:/usr/local/etc/php/php.ini
  ```

### Problèmes connus

#### Operation not permitted: '/var/lib/pgadmin' You may need to manually set the permissions on /var/lib/pgadmin to allow pgadmin to write to it.

Modifier les droits sur le dossier `.pgadmin`:

```bash
chown 5050:5050 -R .pgadmin4
```
#### Erreurs lors de la création de la bd (peut-être dû à une utilisation de Chill antérieure)

```bash
docker-compose exec --user postgres db bash
postgres@fb9290290f50:/$ psql

create database test;
\c test
drop database postgres;
create database postgres;
\c postgres
drop database test;
```

Puis refaire l'import du dump sql
