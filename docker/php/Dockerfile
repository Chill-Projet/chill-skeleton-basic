FROM php:7.4-fpm-buster

ENV POSTGRES_VERSION 14

# default UID for the PHP user
ARG UID=1000
ARG GID=1000

RUN apt update && apt -y --no-install-recommends install wget gnupg  \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - \
   && echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
   && apt update && apt -y --no-install-recommends install \
      libicu-dev \
      g++ \
      postgresql-server-dev-$POSTGRES_VERSION \
      libzip-dev libzip4 unzip \
      libfreetype6-dev \
      libonig-dev `# install oniguruma, required for mbstring` \
      libpng-dev \
      libjpeg62-turbo-dev \
      git \
   && docker-php-ext-configure gd --with-freetype --with-jpeg \
   && docker-php-ext-install -j$(nproc) gd \
   && docker-php-ext-install intl pdo_pgsql mbstring zip bcmath sockets exif \
   && pecl install redis \
   && docker-php-ext-enable redis \
   && apt remove -y wget libicu-dev g++ gnupg libzip-dev \
   && apt autoremove -y \
   && apt purge -y

RUN { \
    echo ""; \
    echo "memory_limit = 512M"; \
    echo ""; \
    } >> /usr/local/etc/php/conf.d/memory_limit.ini

RUN { \
    echo ""; \
    echo "[Date]"; \
    echo "date.timezone = Europe/Brussels"; \
    echo ""; \
    } >> /usr/local/etc/php/conf.d/date.ini

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1

WORKDIR /var/www/app

# ajoute des utilisateurs ayant le uid 1000
RUN groupadd --gid ${GID} "group${GID}" \
   && useradd --uid ${UID} --gid ${GID} --create-home "user${UID}"

CMD ["php-fpm"]

