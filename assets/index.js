/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// styles for assets tests pages
require('./scss/assets_test.scss');

// styles for assets tests pages
require('./scss/test/twig_test.scss');

// start the Stimulus application
// import './bootstrap';
