import SwaggerUI from 'swagger-ui';
import 'swagger-ui/dist/swagger-ui.css';

SwaggerUI({
  url: '/_dev/specs.yaml',
  dom_id: '#swag'
})
