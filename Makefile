THIS_FILE := $(lastword $(MAKEFILE_LIST))
PWD:=$(shell echo ${PWD})
UID:=$(shell id -u)
GID:=$(shell id -g)
DOCKERNODE_CMD=docker run --rm --user ${UID}:${GID} -v ${PWD}:/app --workdir /app -e YARN_CACHE_FOLDER=/app/.yarncache node:16
DOCKER_COMPOSE_PHP_EXEC_CMD=docker-compose run --rm --user $(UID):$(GID) -e CLEAR_CACHE=false -e COMPOSER_HOME=/var/www/app/.composer --entrypoint /usr/bin/env php
DOCKER_PHP_EXEC_CMD_BASE=docker run --rm --user $(UID):$(GID) -v ${PWD}:/var/www/app -e CLEAR_CACHE=false -e COMPOSER_HOME=/var/www/app/.composer --entrypoint /usr/bin/env $(PHP_BASE_IMAGE_TAG)
PHP_BASE_IMAGE=php:8.2-fpm-alpine
PHP_BASE_IMAGE_TARGET=chill_base_php82
PHP_BASE_IMAGE_TAG=chill_php82
NGINX_BASE_IMAGE=nginx
CALVER=$(shell date "+v%Y%m%d%H%M")-${CALVERSION}
ifneq (,$(wildcard ./.env))
    include .env
    export
endif
ifneq (,$(wildcard ./.env.local))
    include .env.local
    export
endif

help:
	@echo "Please make use of 'make <target>' where <target> is one of: "
	@echo "  build-assets           to build assets for production"
	@echo "  build-images           to build image PHP and NGINX"
	@echo "  build-and-push-images  to build image PHP and NGINX and push them on a registry"
	@echo "  init                   to initialize your development directory"
	@echo "  install-vendor         to install vendor using composer"
	@echo "  migrate                to migrate the database to last version"
	@echo "  pull-base-images       to pull base image used in image php and nginx images"
	@echo "  post-install           to run post-install scripts"
	@echo "  post-update            to run post-update scripts"
	@echo "  tag-calver             to replace the current version tag by a calendar version"
	@echo "  tag-calver-push        to replace the current version tag by a calendar version and push to registry"
	@echo "  upgrade-vendors        to upgrade vendor"

build-assets:
	$(DOCKERNODE_CMD) yarn install
	$(DOCKERNODE_CMD) yarn run encore production

init:
	docker build --pull --target $(PHP_BASE_IMAGE_TARGET) --tag $(PHP_BASE_IMAGE_TAG) .
	$(DOCKER_PHP_EXEC_CMD_BASE) composer update --no-scripts --no-interaction
	@$(MAKE) -f $(THIS_FILE) build-assets
	@$(MAKE) -f $(THIS_FILE) post-install

post-install:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) composer run-script post-install-cmd

post-update:
	docker-compose build php
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) composer run-script post-update-cmd

migrate:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) bin/console doctrine:migration:migrate -n

fixtures:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) bin/console doctrine:fixtures:load -n

push-images:
	docker-compose push php nginx

pull-base-images:
	docker pull $(PHP_BASE_IMAGE)
	docker pull $(NGINX_BASE_IMAGE)
	docker pull composer:2

build-and-push-images:
	@$(MAKE) -f $(THIS_FILE) build-base-image-php
	@$(MAKE) -f $(THIS_FILE) build-assets
	docker-compose build php nginx
	@$(MAKE) -f $(THIS_FILE) push-images

build-images:
	@$(MAKE) -f $(THIS_FILE) build-assets
	docker-compose build php nginx

upgrade-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) composer update

install-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) composer install

tag-calver:
	docker tag $(IMAGE_PHP):$(VERSION) $(IMAGE_PHP):$(CALVER)
	docker tag $(IMAGE_NGINX):$(VERSION) $(IMAGE_NGINX):$(CALVER)

tag-calver-push:
	@$(MAKE) -f $(THIS_FILE) tag-calver
	docker push $(IMAGE_PHP):$(CALVER)
	docker push $(IMAGE_NGINX):$(CALVER)

