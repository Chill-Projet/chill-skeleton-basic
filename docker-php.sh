#!/bin/bash

# exécute toujours dans le répertoire `php`
cd $(dirname $0)

if [ $# -eq 0 ]
then
   cmd=/bin/bash
else
   cmd="${@}"
fi

docker-compose exec \
    --user $(id -u):$(id -g) \
    php $cmd
